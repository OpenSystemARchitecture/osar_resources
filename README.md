# OSAR_Resources

## General:
The OsarResources dll shall implement generic used functionalities and resources so not each program should implement these on its own.

## Abbreviations:
OSAR == Open System ARchitecture

## Useful Links:
- [Overall OSAR-Artifactory](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture)
- [OSAR - Resource releases](http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture/Osar_Resources)