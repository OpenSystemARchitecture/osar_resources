pipeline {
  agent {
    node {
        label 'windows'
      }
    }

    environment {
        MSBuildToolsPath = 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe'
    }

    options {
      skipDefaultCheckout(true)
      gitLabConnection('GitLab Public Server')
    }

  stages {
/******************************************************************************************************************************/
/*                                             Start of checkout stage                                                        */
/******************************************************************************************************************************/
    stage('Checkout') {
      steps {
        echo 'Notify GitLab'
        updateGitlabCommitStatus name: 'Checkout', state: 'pending'

        echo 'Checkout git branch'
        cleanWs()
        checkout scm
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Checkout', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Checkout', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                 Start of build stage                                                       */
/******************************************************************************************************************************/
    stage('Build') {
      steps {
        dir('02_Software') {
          echo 'echo Building Visual Studio Solution File'
          updateGitlabCommitStatus name: 'Build', state: 'pending'

          bat '''
            "%MSBuildToolsPath%" OsarResources.sln
          '''
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Build', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Build', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                  Start of test stage                                                       */
/******************************************************************************************************************************/
    // stage('Test') {
    //   steps {
    //     echo 'Testing not configured / available'
    //     updateGitlabCommitStatus name: 'Test', state: 'pending'
    //     //TODO: Add Test State
    //   }
    //   post {
    //     success{
    //       updateGitlabCommitStatus name: 'Test', state: 'success'
    //     }
    //     failure {
    //       updateGitlabCommitStatus name: 'Test', state: 'failed'
    //     }
    //   }
    // }

/******************************************************************************************************************************/
/*                                              Create doxgen documentation                                                   */
/******************************************************************************************************************************/
    stage('Doxygen') {
      steps{
        echo 'Build doxygen documentation'
        updateGitlabCommitStatus name: 'Doxygen', state: 'pending'

        script {
          if(("master" == BRANCH_NAME) || (true == BRANCH_NAME.contains("release/")))
          {
            bat '''
              cd 01_Documentation
              doxygen Doxyfile
              ls -la

              cd html
              7z a doxygen.zip
            '''
          }
          else
          {
            echo 'Do not generate doxygen for non master/release branch'
            bat '''
              mkdir 01_Documentation\\html
              cd 01_Documentation\\html
              copy NUL doxygen.zip
            '''
          }
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Doxygen', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Doxygen', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                  Collect Delivery                                                          */
/******************************************************************************************************************************/
    stage('Collect delivery data') {
      steps {
        echo 'Collect delivery data'
        updateGitlabCommitStatus name: 'Collect delivery data', state: 'pending'

        //>>>>> Create delivery folder <<<<<
        bat '''
          mkdir 99_Delivery
          mkdir 99_Delivery\\bin
          mkdir 99_Delivery\\doc
          mkdir 99_Delivery\\doxygen
        '''

        //>>>>> Download word to pdf builder <<<<<
        script
        {
          withCredentials([usernamePassword(
            credentialsId: 'bca9b7d5-e21b-4205-8471-bf7dce5a432e',
            usernameVariable: 'USERNAME',
            passwordVariable: 'PASSWORD')])
          {
            def server = Artifactory.server 'Riddiks Artifactory Server'
            server.username = "${USERNAME}"
            server.password = "${PASSWORD}"

            def downloadSpec = """{
              "files": [
                {
                  "pattern": "tools/docto/latest/docto.exe",
                  "target": "01_Documentation/",
                  "flat": "true"
                }
              ]}"""
            server.download(downloadSpec)
          }
        }

        //>>>>> Build PDF documentation <<<<<
        bat '''
          cd 01_Documentation
          docto.exe -WD -f TechnicalReference_OsarResources.docx -o TechnicalReference_OsarResources.pdf -t wdFormatPDF
        '''

        //>>>>> Collect build artefacts <<<<<
        bat '''
          copy 01_Documentation\\TechnicalReference_OsarResources.pdf 99_Delivery\\doc\\TechnicalReference_OsarResources.pdf
          copy 01_Documentation\\html\\doxygen.zip 99_Delivery\\doc\\doxygen.zip
          copy 02_Software\\bin\\Debug\\OsarResources.dll 99_Delivery\\bin\\OsarResources.dll
          copy 02_Software\\bin\\Debug\\OsarResources.pdb 99_Delivery\\bin\\OsarResources.pdb
        '''
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Collect delivery data', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Collect delivery data', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                Start of deploy stage                                                       */
/******************************************************************************************************************************/
    stage('Deploy') {
      steps {
        echo 'Start Deploying'
        updateGitlabCommitStatus name: 'Deploy', state: 'pending'

        script{
          String simpleBranchName = BRANCH_NAME.replace("%","").replace(" ","")
          println "simpleBranchName ${simpleBranchName}"

          withCredentials([usernamePassword(
            credentialsId: 'bca9b7d5-e21b-4205-8471-bf7dce5a432e',
            usernameVariable: 'USERNAME',
            passwordVariable: 'PASSWORD')])
          {
            def uploadSpec
            def buildInfo1
            def server = Artifactory.server 'Riddiks Artifactory Server'
            server.username = "${USERNAME}"
            server.password = "${PASSWORD}"

            if("master" == BRANCH_NAME)
            {
              uploadSpec = """{
              "files": [
                          {
                            "pattern": "99_Delivery/",
                            "recursive": "true",
                            "target": "prj-open-system-architecture/Osar_Resources/latest/"
                          }
                        ]}"""
              buildInfo1 = server.upload uploadSpec
              server.publishBuildInfo(buildInfo1)
            }
            else if(true == BRANCH_NAME.contains("release/"))
            {
              uploadSpec = """{
              "files": [
                          {
                            "pattern": "99_Delivery/",
                            "recursive": "true",
                            "target": "prj-open-system-architecture/Osar_Resources/${simpleBranchName}/"
                          }
                        ]}"""

              buildInfo1 = server.upload uploadSpec
              server.publishBuildInfo(buildInfo1)
            }
            else
            {
              println "Do not deploy for non master/release branch"
            }
          }
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Deploy', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Deploy', state: 'failed'
        }
      }
    }
  }
}