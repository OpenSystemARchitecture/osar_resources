﻿/*****************************************************************************************************************************
* @file        Types.cs
* @author      Reinemuth Sebastian
* @date        11-09-2017
* @brief       Generic Types
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarResources.Generic
{
  /*
   * @brief   Generic System State Type with STD_ON / STD_OFF values
   */
  public enum SystemState { STD_ON, STD_OFF };

  public enum NullPointer { NULL_PTR };
}
