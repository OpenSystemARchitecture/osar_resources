/******************************************************************************
 * @file      version.cs
 * @author    OSAR S.Reinemuth
 * @proj      OsarResources
 * @date      Sunday, December 20, 2020
 * @version   Application v. 0.1.11.1
 * @version   Generator   v. 1.2.3.9
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("0.1.11.1")]

namespace OsarResources.Generic
{
  static public class OsarResourcesVersionClass
	{
		public static int major { get; set; }	 //Version of the program
		public static int minor { get; set; }	 //Sub version of the program
		public static int patch { get; set; }	 //Debug patch of the program
		public static int build { get; set; }	 //Count program builds
		
    static OsarResourcesVersionClass()
    {
			major = 0;
			minor = 1;
			patch = 11;
			build = 1;
    }

		public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
	}
}
//!< @version 0.1.1	->	Adding the memory mapping file generation function
//!< @version 0.1.2	->	Adding CRC 32 Calculation Algorithm for the standard Ethernet IEEE 802.3 format >> With the polynom 0x4C11DB7
//!< @version 0.1.3	->	Correct generator output string spelling error
//!< @version 0.1.4	->	Adding new generator resource comments for the doxygen master groups.
//!< @version 0.1.5	->	Adding Osar Log Helper Class with progress string creator function.
//!< @version 0.1.6	->	Adding new standardized Generator Interfaces and Data Types.
//!< @version 0.1.7	->	Adding basic module context interface used in the module dll,
//!< @version 0.1.8	->	Adapted module context interface. Remove "GetInstance". Reason: static functions could not be defined in interfaces.
//!< @version 0.1.9	->	Adapt genInfoType >> Adding new allSeq field and adding timestamp
//!< @version 0.1.10	->	Added new OsarMergeException in OsarResources.Generic
//!< @version 0.1.11	->	Adding a function to generate an UUID
