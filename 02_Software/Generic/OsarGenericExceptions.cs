﻿/*****************************************************************************************************************************
 * @file        OsarGenericExceptions.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.12.2020                                                                                                   *
 * @brief       Implementation of generic osar exceptions                                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarResources.Generic
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarResources.Generic
{
  /**
  * @brief    Generic exceptions of the OSAR System
  * @param    string Message with error informations
  * @retval   none
  */
  [Serializable()]
  public class OsarMergeException : System.Exception
  {
    /**
     * @brief Constructor
     */
    public OsarMergeException() : base() { }

    /**
     * @brief     Constructor
     * @param[in] User Message
     */
    public OsarMergeException(string message) : base(message) { }

    /**
     * @brief     Constructor
     * @param[in] User Message
     * @param[in] Inner exception type
     */
    public OsarMergeException(string message, System.Exception inner) : base(message, inner) { }

    /**
     * @brief   A constructor is needed for serialization when an
     *          exception propagates from a remoting server to the client. 
     */
    protected OsarMergeException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
    { }
  }

  /**
  * @brief    Generic exceptions of the OSAR System
  * @param    string Message with error informations
  * @retval   none
  */
  [Serializable()]
  public class OsarConfigMismatchException : System.Exception
  {
    /**
     * @brief Constructor
     */
    public OsarConfigMismatchException() : base() { }

    /**
     * @brief     Constructor
     * @param[in] User Message
     */
    public OsarConfigMismatchException(string message) : base(message) { }

    /**
     * @brief     Constructor
     * @param[in] User Message
     * @param[in] Inner exception type
     */
    public OsarConfigMismatchException(string message, System.Exception inner) : base(message, inner) { }

    /**
     * @brief   A constructor is needed for serialization when an
     *          exception propagates from a remoting server to the client. 
     */
    protected OsarConfigMismatchException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
    { }
  }

  //Add here further exceptions

}
/**
 * @}
 */