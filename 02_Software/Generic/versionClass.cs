﻿/*****************************************************************************************************************************
* @file        versionClass.cs
* @author      Reinemuth Sebastian
* @date        02-11-2017
* @brief       generic version class
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarResources.Generic
{
  /**
    * @brief    public class of to set an version element
    */
  public class versionClass
  {
    private ushort major;
    private ushort minor;
    private ushort patch;

    public ushort Major
    {
      get
      {
        return major;
      }

      set
      {
        major = value;
      }
    }

    public ushort Minor
    {
      get
      {
        return minor;
      }

      set
      {
        minor = value;
      }
    }

    public ushort Patch
    {
      get
      {
        return patch;
      }

      set
      {
        patch = value;
      }
    }
  }
}
