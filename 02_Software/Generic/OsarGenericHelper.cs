﻿/*****************************************************************************************************************************
* @file        OsarGenericHelper.cs
* @author      Reinemuth Sebastian
* @date        11-09-2017
* @brief       Generic Helper functions
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using OsarResources.XML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarResources.Generic
{
  /**
    * @brief    public class of Osar Generic Helper Functions 
    */
  public static class OsarGenericHelper
  {
    /*
     * @brief    Generic function to check a version
     * @param    string Major Version
     * @param    string Minor Version
     * @param    string Patch Version
     * @param    string Minimum required Major Version
     * @param    string Minimum required Minor Version
     * @param    string Minimum required Patch Version
     * @param    string Maximum allowed Major Version
     * @param    string Maximum allowed Minor Version
     * @param    string Maximum allowed Patch Version
     * @retval   true == Version in range 
     */
    public static bool checkVersionNumber(string Major, string Minor, string Patch, string minMajor, string minMinor, string minPatch, string maxMajor, string maxMinor, string maxPatch)
    {
      UInt32 tempCurrVersion, tempMinVersion, tempMaxVersion;

      /* Caluclate current Version */
      tempCurrVersion = Convert.ToUInt32(Major)*1000000 + Convert.ToUInt32(Minor)*10000 + Convert.ToUInt32(Patch)*100;
      tempMinVersion = Convert.ToUInt32(minMajor) * 1000000 + Convert.ToUInt32(minMinor) * 10000 + Convert.ToUInt32(minPatch) * 100;
      tempMaxVersion = Convert.ToUInt32(maxMajor) * 1000000 + Convert.ToUInt32(maxMinor) * 10000 + Convert.ToUInt32(maxPatch) * 100;

      if( (tempCurrVersion <= tempMaxVersion) && ( tempMinVersion >= tempCurrVersion ) )
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    /*
     * @brief    Generic function to generate a <ModuleName>_MemMap.h file
     * @param    string path to memMap file
     * @param    string module name
     * @param    string author name
     * @retval   true == Version in range 
     */
    public static void generateModuleMemoryMappingFile(string pathToMemMapFile, string moduleName, string authorName)
    {
      string tempFileString;
      string[] tempFileStringList;
      string[] stringSeparators = new string[] { "\r\n" };
      int dummylength;
      string filePath = pathToMemMapFile + "\\";
      string fileName = moduleName + "_MemMap.h";

      /* Create Memory Mapping file */
      /* Read template file */
      tempFileString = OsarResources.Generator.Resources.genericSource.memMapFileHeader;
      tempFileString = tempFileString.Replace("<ModuleName>_MemMap.h", moduleName + "_MemMap.h");
      tempFileString = tempFileString.Replace("<ModuleName>", moduleName);
      tempFileString = tempFileString.Replace("<GeneratorName>", authorName);
      tempFileString = tempFileString.Replace("<Date>", DateTime.Now.ToString());
      tempFileStringList = tempFileString.Split(stringSeparators, StringSplitOptions.None);

      /* Check comment length */
      dummylength = tempFileStringList[0].Length;
      tempFileString = "";
      for (int idx = 0; idx < tempFileStringList.Length; idx++)
      {
        if (tempFileStringList[idx].Contains("/* " + moduleName))
        {
          if (tempFileStringList[idx].Length > dummylength)
          {
            tempFileStringList[idx] = tempFileStringList[idx].Remove(( tempFileStringList[idx].Length - 4 ), 1);
          }
          else
          {
            while (tempFileStringList[idx].Length < dummylength)
            {
              tempFileStringList[idx] = tempFileStringList[idx].Insert(( tempFileStringList[idx].Length - 4 ), " ");
            }
          }
        }

        /* Combine string list again*/
        tempFileString = tempFileString + tempFileStringList[idx] + "\r\n";
      }

      /* Create default config file */
      /* Check if file already exists */
      if (!File.Exists(filePath + fileName)) //Just modify the file if it does not exists
      {
        /* Create and open file to write content */
        File.AppendAllText(filePath + fileName, tempFileString);
      }
    }

    /// <summary>
    /// Converter from XmlFileVersion to UInt64 Type
    /// |   64 - 48   | 47 - 32 | 31 - 16 | 15 - 0
    /// | 0x0000 0000 |  Major  |  Minor  | Patch
    /// </summary>
    /// <param name="xmlFileVersion"></param>
    /// <returns></returns>
    public static UInt64 ConvertXmlFileVersionToU64(XmlFileVersion xmlFileVersion)
    {
      UInt64 version = 0;
      version += xmlFileVersion.PatchVersion;
      version += ( xmlFileVersion.MinorVersion * 65536UL );
      version += ( xmlFileVersion.MajorVersion * 4294967296UL );

      return version;
    }

    /// <summary>
    /// Check if the XmlFileVersions are equal
    /// </summary>
    /// <param name="version1"></param>
    /// <param name="version2"></param>
    /// <returns>
    /// true  == Versions are equal
    /// false == Versions are not equal
    /// </returns>
    public static bool XmlFileVersionEqual(XmlFileVersion version1, XmlFileVersion version2)
    {
      bool retVal = false;
      if (ConvertXmlFileVersionToU64(version1) == ConvertXmlFileVersionToU64(version2))
      {
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /// <summary>
    /// Check if version1 is lower than version 2
    /// </summary>
    /// <param name="version1"></param>
    /// <param name="version2"></param>
    /// <returns>
    /// true  == version1 < version2
    /// false == version1 => version2
    /// </returns>
    public static bool XmlFileVersionLower(XmlFileVersion version1, XmlFileVersion version2)
    {
      bool retVal = false;
      if (ConvertXmlFileVersionToU64(version1) < ConvertXmlFileVersionToU64(version2))
      {
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /// <summary>
    /// Check if version1 is greater than version 2
    /// </summary>
    /// <param name="version1"></param>
    /// <param name="version2"></param>
    /// <returns>
    /// true  == version1 > version2
    /// false == version1 <= version2
    /// </returns>
    public static bool XmlFileVersionGreater(XmlFileVersion version1, XmlFileVersion version2)
    {
      bool retVal = false;
      if (ConvertXmlFileVersionToU64(version1) > ConvertXmlFileVersionToU64(version2))
      {
        retVal = true;
      }
      else
      {
        retVal = false;
      }

      return retVal;
    }

    /// <summary>
    /// Interface to generate a new UUID
    /// </summary>
    /// <returns>string with UUID</returns>
    public static string GenerateANewUUID()
    {
      return Guid.NewGuid().ToString("N");
    }
  }
}
