﻿/*****************************************************************************************************************************
* @file        CRC32Algorithm.cs
* @author      Reinemuth Sebastian
* @date        04-04-2018
* @brief       Containing the basic CRC32 Calculation algorithm
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OsarResources.Security.Checksum.CRC
{
  /**
    * @brief    public class of the basic CRC32 Algorithm
    */
  public class CRC32_0x04C11DB7 : HashAlgorithm
  {
    private uint _currentCrc;
    private readonly bool _isBigEndian = true;

    /*
     * @brief Class constructor
     */
    public CRC32_0x04C11DB7()
    {
      HashSizeValue = 32;
    }

    /*
     * @brief   Class constructor
     * @param   Definition if big or little endian shall be used
     */
    public CRC32_0x04C11DB7(bool isBigEndian = true)  : this()
    {
      _isBigEndian = isBigEndian;
    }

    /*
     * @brief   Computes CRC-32 from multiple buffers.
     * @param   Initial CRC value for the algorithm. It is zero for the first buffer.
     *          Subsequent buffers should have their initial value set to CRC value returned by previous call to this method.
     * @param   Input buffer with data to be checksummed.
     * @param   Offset of the input data within the buffer.
     * @param   Length of the input data in the buffer.
     * @return  Accumulated CRC-32 of all buffers processed so far.
     */
    public static uint Append(uint initial, byte[] input, int offset, int length)
    {
      if (input == null)
        throw new ArgumentNullException("input");
      if (offset < 0 || length < 0 || offset + length > input.Length)
        throw new ArgumentOutOfRangeException("length");
      return AppendInternal(initial, input, offset, length);
    }

    /*
     * @brief   Computes CRC-32 from multiple buffers.
     * @param   Initial CRC value for the algorithm. It is zero for the first buffer.
     * @param   Input buffer with data to be checksummed.
     * @return  Accumulated CRC-32 of all buffers processed so far.
     */
    public static uint Append(uint initial, byte[] input)
    {
      if (input == null)
        throw new ArgumentNullException();
      return AppendInternal(initial, input, 0, input.Length);
    }

    /*
     * @brief   Computes CRC-32 from input buffer.
     * @param   Input buffer containing data to be checksummed.
     * @param   Offset of the input data within the buffer.
     * @param   Length of the input data in the buffer.
     * @return  CRC-32 of the data in the buffer.
     */
    public static uint Compute(byte[] input, int offset, int length)
    {
      return Append(0, input, offset, length);
    }

    /*
     * @brief   Computes CRC-32 from input buffer.
     * @param   Input buffer containing data to be checksummed.
     * @return  CRC-32 of the data in the buffer.
     */
    public static uint Compute(byte[] input)
    {
      return Append(0, input);
    }

    /*
     * @brief   Computes CRC-32 from input buffer and writes it after end of data (buffer should have 4 bytes reserved space for it).
     * @param   Input buffer with data to be checksummed.
     * @param   Offset of the input data within the buffer.
     * @param   Length of the input data in the buffer.
     * @return  CRC-32 of the data in the buffer.
     */
    public static uint ComputeAndWriteToEnd(byte[] input, int offset, int length)
    {
      if (length + 4 > input.Length)
        throw new ArgumentOutOfRangeException("length", "Length of data should be less than array length - 4 bytes of CRC data");
      var crc = Append(0, input, offset, length);
      var r = offset + length;
      input[r] = (byte)crc;
      input[r + 1] = (byte)( crc >> 8 );
      input[r + 2] = (byte)( crc >> 16 );
      input[r + 3] = (byte)( crc >> 24 );
      return crc;
    }

    /*
     * @brief   Computes CRC-32 from input buffer - 4 bytes and writes it as last 4 bytes of buffer.
     * @param   Input buffer with data to be checksummed.
     * @return  CRC-32 of the data in the buffer.
     */
    public static uint ComputeAndWriteToEnd(byte[] input)
    {
      if (input.Length < 4)
        throw new ArgumentOutOfRangeException("input", "Input array should be 4 bytes at least");
      return ComputeAndWriteToEnd(input, 0, input.Length - 4);
    }

    /*
     * @brief   Validates correctness of CRC-32 data in source buffer with assumption that CRC-32 data located at end of buffer in reverse bytes order. Can be used in conjunction with
     * @param   Input buffer with data to be checksummed.
     * @return  Is checksum valid.
     */
    public static bool IsValidWithCrcAtEnd(byte[] input, int offset, int lengthWithCrc)
    {
      return Append(0, input, offset, lengthWithCrc) == 0x2144DF1C;
    }

    /*
     * @brief   Appends CRC-32 from given buffer
     * @param   None
     * @return  None
     */
    public override void Initialize()
    {
      _currentCrc = 0;
    }

    protected override void HashCore(byte[] input, int offset, int length)
    {
      _currentCrc = AppendInternal(_currentCrc, input, offset, length);
    }

    protected override byte[] HashFinal()
    {
      if (_isBigEndian)
        return new[] { (byte)( _currentCrc >> 24 ), (byte)( _currentCrc >> 16 ), (byte)( _currentCrc >> 8 ), (byte)_currentCrc };
      else
        return new[] { (byte)_currentCrc, (byte)( _currentCrc >> 8 ), (byte)( _currentCrc >> 16 ), (byte)( _currentCrc >> 24 ) };
    }

    private static readonly CRC32_0x04C11DB7_SafeProxy _proxy = new CRC32_0x04C11DB7_SafeProxy();

    private static uint AppendInternal(uint initial, byte[] input, int offset, int length)
    {
      if (length > 0)
      {
        return _proxy.Append(initial, input, offset, length);
      }
      else
        return initial;
    }
  }
}
