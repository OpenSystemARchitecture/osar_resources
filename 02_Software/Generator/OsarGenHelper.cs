﻿/*****************************************************************************************************************************
* @file        OsarGenHelper.cs
* @author      Reinemuth Sebastian
* @date        11-09-2017
* @brief       Helper functions for source code generator
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OsarResources.XML;

namespace OsarResources.Generator
{
  /**
    * @brief    public class of Osar Generator Helper Functions 
    */
  public static class OsarGenHelper
  {
    /**
    * @brief    Check XML File Version with allowed range
    * @param    XmlFileVersion XML File Version
    * @param    string Minimum requiered Major Version
    * @param    string Minimum requiered Minor Version
    * @param    string Minimum requiered Patch Version
    * @param    string Maximum allowed Major Version
    * @param    string Maximum allowed Minor Version
    * @param    string Maximum allowed Patch Version
    * @retval   true == Version in range    false == Version not in range
    */
    public static Boolean checkXmlFileVersion(XmlFileVersion xmlFileVersion, string minMajor, string minMinor, string minPatch, string maxMajor, string maxMinor, string maxPatch)
    {
      if( ( Convert.ToUInt16(maxMajor) <= xmlFileVersion.MajorVersion ) && ( Convert.ToUInt16(minMajor) >= xmlFileVersion.MajorVersion ) &&
          ( Convert.ToUInt16(maxMinor) <= xmlFileVersion.MinorVersion ) && ( Convert.ToUInt16(minMinor) >= xmlFileVersion.MinorVersion ) &&
          ( Convert.ToUInt16(maxPatch) <= xmlFileVersion.PatchVersion ) && ( Convert.ToUInt16(minPatch) >= xmlFileVersion.PatchVersion ) )
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    /**
    * @brief    Interface to merge to gen info types
    * @param    GenInfoType Gen Info 1
    * @param    GenInfoType Gen Info 2
    * @retval   Merged info types
    */
    public static GenInfoType MergeGenInfoType(GenInfoType genInfo1, GenInfoType genInfo2)
    {
      GenInfoType genInfoMerge;
      genInfoMerge.info = new List<string>();
      genInfoMerge.error = new List<string>();
      genInfoMerge.warning = new List<string>();
      genInfoMerge.log = new List<string>();
      genInfoMerge.allSeq = new List<string>();

      /* Merge log */
      if (null != genInfo1.log && null != genInfo2.log)
      {
        genInfoMerge.log.AddRange(genInfo1.log);
        genInfoMerge.log.AddRange(genInfo2.log);
      }
      else if (null != genInfo1.log)
      {
        genInfoMerge.log.AddRange(genInfo1.log);
      }
      else if (null != genInfo2.log)
      {
        genInfoMerge.log.AddRange(genInfo2.log);
      }
      else
      {
        //Do nothing >> Both are empty
      }

      /* Merge Error */
      if (null != genInfo1.error && null != genInfo2.error)
      {
        genInfoMerge.error.AddRange(genInfo1.error);
        genInfoMerge.error.AddRange(genInfo2.error);
      }
      else if (null != genInfo1.error)
      {
        genInfoMerge.error.AddRange(genInfo1.error);
      }
      else if (null != genInfo2.error)
      {
        genInfoMerge.error.AddRange(genInfo2.error);
      }
      else
      {
        //Do nothing >> Both are empty
      }

      /* Merge Warning */
      if (null != genInfo1.warning && null != genInfo2.warning)
      {
        genInfoMerge.warning.AddRange(genInfo1.warning);
        genInfoMerge.warning.AddRange(genInfo2.warning);
      }
      else if (null != genInfo1.warning)
      {
        genInfoMerge.warning.AddRange(genInfo1.warning);
      }
      else if (null != genInfo2.warning)
      {
        genInfoMerge.warning.AddRange(genInfo2.warning);
      }
      else
      {
        //Do nothing >> Both are empty
      }

      /* Merge Info */
      if (null != genInfo1.info && null != genInfo2.info)
      {
        genInfoMerge.info.AddRange(genInfo1.info);
        genInfoMerge.info.AddRange(genInfo2.info);
      }
      else if (null != genInfo1.info)
      {
        genInfoMerge.info.AddRange(genInfo1.info);
      }
      else if (null != genInfo2.info)
      {
        genInfoMerge.info.AddRange(genInfo2.info);
      }
      else
      {
        //Do nothing >> Both are empty
      }

      /* Merge all sequential */
      if (null != genInfo1.allSeq && null != genInfo2.allSeq)
      {
        genInfoMerge.allSeq.AddRange(genInfo1.allSeq);
        genInfoMerge.allSeq.AddRange(genInfo2.allSeq);
      }
      else if (null != genInfo1.allSeq)
      {
        genInfoMerge.allSeq.AddRange(genInfo1.allSeq);
      }
      else if (null != genInfo2.allSeq)
      {
        genInfoMerge.allSeq.AddRange(genInfo2.allSeq);
      }
      else
      {
        //Do nothing >> Both are empty
      }

      return genInfoMerge;
    }
  }
}
