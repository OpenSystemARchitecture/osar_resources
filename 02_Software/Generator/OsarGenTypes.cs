﻿/*****************************************************************************************************************************
 * @file        OsarGenTypes.cs                                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the different types which are needed by the standardized OSAR Generator                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup OsarResources.Generator
 * @{
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarResources.Generator
{
  /// <summary>
  /// General Information Type to interact with an standardized OSAR Generator
  /// </summary>
  public struct GenInfoType
  {
    public List<String> info;
    public List<String> warning;
    public List<String> error;
    public List<String> log;
    public List<String> allSeq;

    #region Log Interfaces
    /// <summary>
    /// Interface to add an Log message to the Gen Info Type Structure
    /// </summary>
    /// <param name="msg"></param>
    public void AddLogMsg(string msg)
    {
      string message = "";

      if(null == log)
      {
        log = new List<string>();
      }

      if("" != msg)
      {
        message += OsarResources.Generator.Resources.generatorOutputs.Std_Output;
        message += "[LOGGING] ";
        message += DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " ";
        message += msg;

        log.Add(message);
        AddSeqMsg(message);
      }
    }
    #endregion

    #region Info Interfaces
    /// <summary>
    /// Interface to add an information message to the Gen Info Type Structure
    /// </summary>
    /// <param name="msg"></param>
    public void AddInfoMsg(string msg)
    {
      string message = "";

      if (null == info)
      {
        info = new List<string>();
      }

      if ("" != msg)
      {
        message += OsarResources.Generator.Resources.generatorOutputs.Std_Output;
        message += "[INFO] ";
        message += DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " ";
        message += msg;

        info.Add(message);
        AddSeqMsg(message);
      }
    }
    #endregion

    #region Warning Interfaces
    /// <summary>
    /// Interface to add an warning message to the Gen Info Type Structure
    /// </summary>
    /// <param name="warningId"></param>
    /// <param name="msg"></param>
    public void AddWaringMsg(UInt16 warningId, string msg)
    {
      string message = "";

      if (null == warning)
      {
        warning = new List<string>();
      }

      if ("" != msg)
      {
        message += OsarResources.Generator.Resources.generatorOutputs.Std_Output;
        message += "[WARNING] ";
        message += DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " ";
        message += warningId.ToString() + " ";
        message += msg;

        warning.Add(message);
        AddSeqMsg(message);
      }
    }
    #endregion

    #region Error Interfaces
    /// <summary>
    /// Interface to add an warning message to the Gen Info Type Structure
    /// </summary>
    /// <param name="warningId"></param>
    /// <param name="msg"></param>
    public void AddErrorMsg(UInt16 errorId, string msg)
    {
      string message = "";

      if (null == error)
      {
        error = new List<string>();
      }

      if ("" != msg)
      {
        message += OsarResources.Generator.Resources.generatorOutputs.Std_Output;
        message += "[ERROR] ";
        message += DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " ";
        message += errorId.ToString() + " ";
        message += msg;

        error.Add(message);

        AddSeqMsg(message);
      }
    }
    #endregion

    #region All sequential Interface
    /// <summary>
    /// Interface to add a sequential msg
    /// </summary>
    /// <param name="msg"></param>
    private void AddSeqMsg(string msg)
    {
      if (null == allSeq)
      {
        allSeq = new List<string>();
      }

      allSeq.Add(msg);
    }
    #endregion
  }
}

/**
* @}
*/
