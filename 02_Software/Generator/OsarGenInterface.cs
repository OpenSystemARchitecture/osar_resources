﻿/*****************************************************************************************************************************
 * @file        OsarGenTypes.cs                                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the different types which are needed by the standardized OSAR Generator                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup OsarResources.Generator
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using System.Windows.Controls;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarResources.Generator
{
  /// <summary>
  /// Standardized Interface for an OSAR module generator
  /// </summary>
  public interface OsarModuleGeneratorInterface
  {
    /// <summary>
    /// Getter / Setter for the Configuration File Version
    /// </summary>
    XmlFileVersion XmlFileVersion { get; set; }

    /// <summary>
    /// Interface to validate the active configuration
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info     >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors   >> General validation errors
    /// List<string> errors   >> Validation logs
    /// </returns>
    GenInfoType ValidateConfiguration();

    /// <summary>
    /// Interface to generate the active configuration
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info     >> General generation information
    /// List<string> warnings >> General generation warnings
    /// List<string> errors   >> General generation errors
    /// List<string> errors   >> generation logs
    /// </returns>
    GenInfoType GenerateConfiguration();

    /// <summary>
    /// Interface to set a default configuration
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info     >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors   >> General validation errors
    /// List<string> errors   >> Validation logs
    /// </returns>
    GenInfoType SetDefaultConfiguration();

    /// <summary>
    /// Interface to update an outdated configuration to an up to date configuration version
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info     >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors   >> General validation errors
    /// List<string> errors   >> Validation logs
    /// </returns>
    GenInfoType UpdateConfigurationFileVersion();

    /// <summary>
    /// /// Interface to update an outdated configuration to an specific configuration version
    /// </summary>
    /// <param name="updateToVersion"> Specific configuration version </param>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info     >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors   >> General validation errors
    /// List<string> errors   >> Validation logs
    /// </returns>
    GenInfoType UpdateConfigurationFileVersion(XmlFileVersion updateToVersion);
  }

  /// <summary>
  /// Standardized Interface for an OSAR module generator
  /// </summary>
  public interface OsarModuleContextInterface
  {
    /// <summary>
    /// Interface function to get the active view with active version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <returns>Return of view object</returns>
    UserControl GetView(string pathToCfgFile, string absPathToBaseModuleFolder);

    /// <summary>
    /// Interface function to get the active view with specific version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <param name="viewVersion">Version of return view object</param>
    /// <returns>Return of view object</returns>
    UserControl GetView(string pathToCfgFile, string absPathToBaseModuleFolder, XmlFileVersion viewVersion);

    /// <summary>
    /// Interface function to get the active view model with active version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <returns>Return of view model object</returns>
    OsarModuleGeneratorInterface GetViewModel(string pathToCfgFile, string absPathToBaseModuleFolder);

    /// <summary>
    /// Interface function to get the active view model with specific version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <param name="viewVersion">Version of return view model object</param>
    /// <returns>Return of view object</returns>
    OsarModuleGeneratorInterface GetViewModel(string pathToCfgFile, string absPathToBaseModuleFolder, XmlFileVersion viewVersion);

    /// <summary>
    /// Interface function to get the active configuration file version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <returns>Active file version</returns>
    XmlFileVersion GetActiveVersion(string pathToCfgFile, string absPathToBaseModuleFolder);

    /// <summary>
    /// Interface function to get a list with available versions
    /// </summary>
    /// <returns>List with available versions</returns>
    List<XmlFileVersion> GetAvailableVersions();

    /// <summary>
    /// Interface to convert the active configuration file to a specific version
    /// </summary>
    /// <param name="pathToCfgFile">Path to configuration file</param>
    /// <param name="absPathToBaseModuleFolder">Absolute path to module base folder</param>
    /// <param name="fileVersion"></param>
    void ConvertCfgFileToSpecificVersion(string pathToCfgFile, string absPathToBaseModuleFolder, XmlFileVersion fileVersion);
  }
}
/**
* @}
*/
