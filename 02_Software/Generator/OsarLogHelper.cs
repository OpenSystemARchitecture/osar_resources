﻿/*****************************************************************************************************************************
* @file        OsarLogHelper.cs
* @author      Reinemuth Sebastian
* @date        25-03-2019
* @brief       Helper functions for log strings generator
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarResources.Generator
{
  /**
    * @brief    public class of Osar Logging Helper class
    */
  public static class OsarLogHelper
  {
    /**
    * @brief    Create a progress console  log information
    * @param    Int >> Count of max elements
    * @param    Int >> Count of processed elements
    * @retval   string >> Progress bar string
    * @note     Output >> "> x%"
    */
    public static string createProgressInformation(int cntElements, int activeElement)
    {
      string retVal = "";
      double activeProgress;

      activeProgress = ( 100.0 / (double)cntElements ) * (double)activeElement;

      retVal = "\r" + OsarResources.Generator.Resources.generatorOutputs.Std_Output + ( (int)activeProgress ).ToString() + "%";

      return retVal;
    }
  }
}
