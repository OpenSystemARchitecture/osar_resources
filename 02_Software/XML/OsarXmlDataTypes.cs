﻿/*****************************************************************************************************************************
* @file        OsarXmlDataTypes.cs
* @author      Reinemuth Sebastian
* @date        11-09-2017
* @brief       Containing XML DataTypes
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OsarResources.XML
{
  /**
    * @struct   XmlFileVersion XML Configuration file version
    * @brief    Structure of version content from xml file version
    */
  public struct XmlFileVersion
  {
    public UInt16 MajorVersion;
    public UInt16 MinorVersion;
    public UInt16 PatchVersion;
  }
}
