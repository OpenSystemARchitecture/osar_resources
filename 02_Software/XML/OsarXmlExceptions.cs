﻿/*****************************************************************************************************************************
* @file        OsarXmlExceptions.cs
* @author      Reinemuth Sebastian
* @date        11-09-2017
* @brief       Containing XML Exeptions
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using System;

namespace OsarResources.XML
{
  /**
    * @brief    Invalid Xml Argument Structure / Content detected
    * @param    string Messge with error informations
    * @retval   none
    */
  [Serializable()]
  public class InvalidXmlArgumentException : System.Exception
  {
    public InvalidXmlArgumentException() : base() { }
    public InvalidXmlArgumentException(string message) : base(message) { }
    public InvalidXmlArgumentException(string message, System.Exception inner) : base(message, inner) { }

    // A constructor is needed for serialization when an
    // exception propagates from a remoting server to the client. 
    protected InvalidXmlArgumentException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
    { }
  }


  /**
    * @brief    Invalid Xml Version Structure / Content detected
    * @param    string Messge with error informations
    * @retval   none
    */
  [Serializable()]
  public class InvalidXmlVersionException : System.Exception
  {
    public InvalidXmlVersionException() : base() { }
    public InvalidXmlVersionException(string message) : base(message) { }
    public InvalidXmlVersionException(string message, System.Exception inner) : base(message, inner) { }

    // A constructor is needed for serialization when an
    // exception propagates from a remoting server to the client. 
    protected InvalidXmlVersionException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
    { }
  }
}
