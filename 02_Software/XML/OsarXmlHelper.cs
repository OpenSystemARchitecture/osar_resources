﻿/*****************************************************************************************************************************
* @file        OsarXmlHelper.cs
* @author      Reinemuth Sebastian
* @date        11-09-2017
* @brief       Containing XML Helper Functions
* @version     1.0.0
* last checkin :
* $Author: sreinemuth $ @ $Date: 2017-02-18 12:31:21 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 14 $
****************************************************************************************************************************/
using OsarResources.XML.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using OsarResources;

namespace OsarResources.XML
{
  /**
    * @brief    public class of Osar XML Helper Functions 
    */
  static public class OsarXmlHelper
  {
    /**
    * @brief    Reading an Xml Tag with attributes min / max / value
    * @details  The function would read from a given BASE XML ELEMENT the min max values and compare them with its value 
    * @param    XML Reader
    * @retval   uint16 check min max value
    * @note     Used Resources:
    *           - genericXml.AttributeValue
    *           - genericXml.AttributeMinValue
    *           - genericXml.AttributeMaxValue
    */
    static public UInt16 readXmlTagAttrUint16MinMaxVal(XmlReader xmlDocReader)
    {
      UInt16 minVal = 1, maxVal = 0, uint16Value = 0;

      /* >> Read parameters << */
      if (xmlDocReader.HasAttributes)
      {
        while (xmlDocReader.MoveToNextAttribute())
        {
          if (xmlDocReader.Name == genericXml.AttributeMaxValue)
          {
            maxVal = Convert.ToUInt16(xmlDocReader.Value);  // Set temporary maximum value 
          }
          else if (xmlDocReader.Name == genericXml.AttributeMinValue)
          {
            minVal = Convert.ToUInt16(xmlDocReader.Value);  // Set temporary minimum value 
          }
          if (xmlDocReader.Name == genericXml.AttributeValue)
          {
            uint16Value = Convert.ToUInt16(xmlDocReader.Value);  // Set temporary minimum value 
          }
        }
      }

      /* >> Check Parameter range  and set ID << */
      if (( minVal <= maxVal ) && ( uint16Value >= minVal ) && ( uint16Value <= maxVal ))
      {
        return uint16Value; /* Return checked Element */
      }
      else
      {
        /* Throw exception for invalid xml arguments s*/
        throw new XML.InvalidXmlArgumentException("Value argument dose not fit into the configured min /max values or they are not available. Min value: " + minVal
                  + " Max value: " + maxVal + " Value: " + uint16Value);
      }
    }

    /**
    * @brief    Reading an Xml Tag with attribute Status value 
    * @details  The function would read from an given BASE XML ELEMENT the status values and compare them with allowed strings
    * @param    XML Reader
    * @retval   string with spell checked status
    * @note     Used Resources:
    *           - genericXml.AttributeValue
    *           - genericXml.ValueStatusStdOn
    *           - genericXml.ValueStatusStdOff
    */
    static public string readXmlTagAttrStringStatus(XmlReader xmlDocReader)
    {
      string dummyStatus = "";

      /* >> Read parameters << */
      if (xmlDocReader.HasAttributes)
      {
        while (xmlDocReader.MoveToNextAttribute())
        {
          if (xmlDocReader.Name == genericXml.AttributeValue)
          {
            dummyStatus = xmlDocReader.Value;  // Set temporary status string
          }
        }
      }

      /* >> Check Parameter range  and set ID << */
      if (( dummyStatus == genericXml.ValueStatusStdOn ) || ( dummyStatus == genericXml.ValueStatusStdOff ))
      {
        return dummyStatus; /* Return checked Element */
      }
      else
      {
        /* Throw exception for invalid xml arguments s*/
        throw new XML.InvalidXmlArgumentException("Value argument dose not fit into the allowed or they are not available. Allowed Values: " + genericXml.ValueStatusStdOn
          + " /  " + genericXml.ValueStatusStdOff + " Configured value = " + dummyStatus);
      }
    }

    /**
    * @brief    Reading the XML BASE TAG "XmlFileVersion"
    * @details  The function would read from an given BASE XML ELEMENT the version information
    * @param    XML Reader
    * @note     ------------------
    *           - XmlFileVersion -     <>Given Elements 
    *           ------------------
    *                   |
    *                   ---MajorVersion
    *                   |       |
    *                   |       --- Value
    *                   |
    *                   ---MinorVersion
    *                   |       |
    *                   |       --- Value
    *                   |
    *                   ---PatchVersion
    *                           |
    *                           --- Value
    * 
    * @retval   string with spell checked status
    */
    static public void readXmlTagXmlFileVersion(XmlReader xmlDocReader, out XmlFileVersion xmlVersion)
    {
      try
      {
        /* Set Default values */
        xmlVersion.MajorVersion = 0;
        xmlVersion.MinorVersion = 0;
        xmlVersion.PatchVersion = 0;

        /* Reading Xml Version Tag Base Tag */
        while (xmlDocReader.Read())
        {
          /* Select Node Type Elements */
          if (xmlDocReader.NodeType == XmlNodeType.Element)
          {
            if (xmlDocReader.Name == genericXml.TagGeneralVersionMajor)
            {
              xmlDocReader.Read();
              xmlVersion.MajorVersion = Convert.ToUInt16(xmlDocReader.Value);  // Set major version
            }
            else if (xmlDocReader.Name == genericXml.TagGeneralVersionMinor)
            {
              xmlDocReader.Read();
              xmlVersion.MinorVersion = Convert.ToUInt16(xmlDocReader.Value);  // Set major version
            }
            else if (xmlDocReader.Name == genericXml.TagGeneralVersionPatch)
            {
              xmlDocReader.Read();
              xmlVersion.PatchVersion = Convert.ToUInt16(xmlDocReader.Value);  // Set patch version
            }
          }
          else if (xmlDocReader.NodeType == XmlNodeType.EndElement)
          {
            /*------------------------ Process General Node element -------------------------------------*/
            if (xmlDocReader.Name == genericXml.TagGeneralVersion)  /* Process Det Module Version */
            {
              break;
            }
          }
        }
      }
      catch(Exception ex)
      {
        throw new InvalidXmlVersionException("Version structure dose not fit into the needed structure. Original Error message: " + ex);
      }
    }
  }
}
